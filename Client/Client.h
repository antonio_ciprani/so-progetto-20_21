#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <termios.h>
#include <stddef.h>

#include "linked_list.h"

#define PORT 8080
#define ENTER 10
#define TAB 9
#define DELETE 127
#define BCKSPC 8
#define MAXLENGHT 50
#define MSGLENGHT 256



typedef struct User{
    char username[MAXLENGHT];
    char password[MAXLENGHT];
    int connected;
    struct sockaddr_in cliaddr;

}User ;

typedef struct UserListItem {
  ListItem list;
  User user;
  int idx;

} UserListItem;

typedef struct Message{
  char sender[MAXLENGHT];
  char reciver[MAXLENGHT];
  char data[MSGLENGHT];

}Message;

typedef union Payload{
  User user;
  Message msg;
}Payload;

typedef struct Transfer{
  int op;
  Payload payload;
}Transfer;


typedef struct Inbox{
  ListItem list;
  Message msg;
}Inbox;

typedef struct thread_args_s{
  User* user;

  int sockfd;
  struct sockaddr_in* servaddr;
  ListHead* inbox;
} thread_args_t;

int authentication(User* user, int sockfd, struct sockaddr_in  servaddr, char username[MAXLENGHT]);

void takeinput(char ch[MAXLENGHT]);

void takepwd(char pwd[MAXLENGHT]);

void takemsg(char ch[MSGLENGHT]);

int ugetchar(void);

//password masking 

char getch(void);

int recv_list(int sockfd,struct sockaddr_in  servaddr, ListHead* head,int size, char username[MAXLENGHT]);

void* reciving(void * args);

void Init_msg(Message* msg, char data[MSGLENGHT],int id, ListHead* head, char username[MAXLENGHT]);
UserListItem* List_find_by_username(ListHead* head, char username[MAXLENGHT]);
void store(thread_args_t *targs, Message *msg);
char *input(thread_args_t *targs);