#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <termios.h>
#include <assert.h>
#include <pthread.h>
#include <stddef.h>


#include "Client.h"


static struct termios old, new;

/* Initialize new terminal i/o settings */
void initTermios(int echo)
{
    tcgetattr(0, &old); //grab old terminal i/o settings
    new = old; //make new settings same as old settings
    new.c_lflag &= ~ICANON; //disable buffered i/o
    new.c_lflag &= echo ? ECHO : ~ECHO; //set echo mode
    tcsetattr(0, TCSANOW, &new); //apply terminal io settings
}

/* Restore old terminal i/o settings */
void resetTermios(void)
{
    tcsetattr(0, TCSANOW, &old);
}

/* Read 1 character - echo defines echo mode */
char getch_(int echo)
{
    char ch;
    initTermios(echo);
    ch = getchar();
    resetTermios();
    return ch;
}

/* 
Read 1 character without echo 
getch() function definition.
*/
char getch(void)
{
    return getch_(0);
}

//function responsible of taking the username input from the user
void takeinput(char ch[MAXLENGHT]){
    fgets(ch,MAXLENGHT,stdin);
    ch[strlen(ch) - 1] = '\0';

}

//function responsible of taking the message input from the user 
void takemsg(char ch[MSGLENGHT]){
    fgets(ch,MSGLENGHT,stdin);
    ch[strlen(ch) - 1] = '\0';

}

void Print_msg(ListHead* head){
    if(head->size == 0){
        printf("\nNo messages to show");
        return;
    }
    
    
    ListItem * aux = head->first;
    printf("\nIncoming messages:\n");
    while(aux){
        Inbox* initem = (Inbox*)aux;
        printf("\n\t\t\t[%s]\t%s\n",initem->msg.sender,initem->msg.data);


        aux = aux->next;
    }


}

//function responsible of taking the password input from the user
void takepwd(char pwd[MAXLENGHT]){
    int i = 0;
    char ch;
    while(1){
        ch = getch();
        if(ch == ENTER || ch == TAB){
            pwd[i] = '\0';
            break;
        }
        else if(ch == BCKSPC || ch == DELETE){
            if(i > 0){
                pwd[i--] = '\b';
            }
        }
        else{
            pwd[i++] = ch;
            
        
        }
    }
    


}

//function that handles the authentication pashe(signup&login)
int authentication(User* user, int sockfd, struct sockaddr_in  servaddr, char username[MAXLENGHT]){
    int op, res, written_bytes, read_bytes;
    char pwd2[MAXLENGHT];
    
    socklen_t addrlen = sizeof(servaddr);
    size_t len;
    Transfer tran;



    printf("\n\t\t\t\t----------Login----------");
    printf("\nPlease choose an option: ");
    printf("\n1.Sign up!");
    printf("\n2.Login!");


    printf("\nYour choice:\t");
    scanf("%d",&op);
    fgetc(stdin);

    /* written_bytes = sendto(sockfd,(int*)&op,sizeof(int),MSG_CONFIRM,(const struct sockaddr *)&servaddr, sizeof(servaddr));
    if(written_bytes == -1)
    {
        perror("An error occurred while sending op to the Server");
        exit(1);
    } */

    tran.op = op;

    len = offsetof(Transfer,payload);

    written_bytes = 0;
    switch(op){
    case 1:
        
        printf("\nEnter your Username:\t");
        takeinput(user->username);
        strcpy(username,user->username);
        
        printf("\nEnter your password:\t");
        takepwd(user->password);

        printf("\nConfirm your password:\t");
        takepwd(pwd2);

        if(!strcmp(user->password, pwd2))
        {

            /* written_bytes = sendto(sockfd,user,sizeof(User),MSG_CONFIRM,(const struct sockaddr *)&servaddr, sizeof(servaddr));
            if(written_bytes == -1)
            {
                perror("An error occurred while sending User to the Server");
                exit(1);
            } */

            tran.payload.user = *user;

            len += sizeof(User);

            written_bytes = sendto(sockfd,&tran,len,MSG_WAITALL,(const struct sockaddr *)&servaddr, sizeof(servaddr));
            if(written_bytes == -1){
                perror("An error occurred while sending User to the Server");
                exit(1);
            } 



            read_bytes = recvfrom(sockfd, (int *)&res, sizeof(int), MSG_WAITALL,( struct  sockaddr *)&servaddr, &addrlen);
            if(read_bytes == -1){
                perror("An error occurred while reciving ack from the Server");
                exit(1);
            }

            if(res == 0){
                printf("\nUser successfully signed up!\n");
                
                return 0;
            }

            else if(res == 1){
                printf("\nAn error occured while Siging up :(");
                return 1;
            }
        }
        else
        {
            printf("\nPassword do not match!");
            return 1;
        }
        
        
    case 2:
    
        printf("\nEnter your Username:\t");
        takeinput(user->username);
        strcpy(username,user->username);

        printf("\nEnter your password:\t");
        takepwd(user->password);

        tran.payload.user = *user;

        /* written_bytes = sendto(sockfd,user,sizeof(User),MSG_CONFIRM,(const struct sockaddr *)&servaddr, sizeof(servaddr));
        if(written_bytes == -1)
        {
            perror("An error occurred while sending User to the Server");
            exit(1);
        } */

        len += sizeof(User);

        written_bytes = sendto(sockfd,&tran,len,MSG_WAITALL,(const struct sockaddr *)&servaddr, sizeof(servaddr));
        if(written_bytes == -1){
            perror("An error occurred while sending User to the Server");
            exit(1);
        } 

        read_bytes = recvfrom(sockfd, (int *)&res, sizeof(int), MSG_WAITALL,( struct  sockaddr *)&servaddr, &addrlen);
        if(read_bytes == -1)
        {
            perror("An error occurred while reciving ack from the Server");
            exit(1);
        }

        if(res == 0){
            printf("\nUser successfully logged in!\n");
            return 0;
        }
        else if(res == 1){
            printf("\nWrong Password!\n");
            return 1;
        }
        else if(res == 2){
            printf("\nInvalid Username\n");
            return 1;
        }

    default:
        fprintf(stderr," unknown command -- op=%d\n",tran.op);
        return 1;
        
                      

    }

}

void UList_init(UserListItem* uitem, char username[MAXLENGHT]){
    strcpy(uitem->user.username,username);

}

void UserList_print(ListHead * head){
    ListItem* aux = head->first;
    while(aux){
        UserListItem * uitem = (UserListItem*) aux;
        printf("\n%d.%s",uitem->idx, uitem->user.username);
        printf("\n");
        aux = aux->next;
    }
    
}

UserListItem* List_find_by_username(ListHead* head, char username[MAXLENGHT]){
    ListItem* aux = head->first;
    while(aux){
        UserListItem* uitem = (UserListItem*) aux;
        if(!strcmp(uitem->user.username,username))
            return uitem;
        aux = aux->next;
    }
    return 0;
}

//function that removes the current user from the online users list recived from the server
void Remove_CurrentUser(ListHead* head, char username[MAXLENGHT]){
    UserListItem* uitem = List_find_by_username(head,username);
    if(uitem != 0){
        ListItem* result = List_detach(head,(ListItem*)uitem);
        assert(result);
    }
    
}

//function responsible of recving the online users list from the server 
int recv_list(int sockfd,struct sockaddr_in  servaddr, ListHead* head,int size, char username[MAXLENGHT]){
    UserListItem* uitem;
    char onuser[MAXLENGHT];
    socklen_t len = sizeof(servaddr);
    int read_bytes;
    int idx = 1;   
    
    if(size == 1){
        return 1;
    }
   
    for(int i=0;i<size;i++){
        
        read_bytes = recvfrom(sockfd,(char *)onuser,sizeof(onuser),0,(struct sockaddr*)&servaddr,&len );
        if(read_bytes == -1)
        {
            perror("An error occurred while reciving username from the Server");
            exit(1);
        }
        onuser[read_bytes] = '\0';
        if(List_find_by_username(head,onuser) == 0){
            uitem = malloc(sizeof(UserListItem));
            memset(uitem,0,sizeof(UserListItem));
            UList_init(uitem,onuser);       
            ListItem* result = List_insert(head,head->last,(ListItem*)uitem);
            assert(result);
            
        }
        memset(onuser,0,sizeof(onuser));   
        
    }
    Remove_CurrentUser(head,username);
    ListItem* aux = head->first;
    while(aux){
        UserListItem* uitem = (UserListItem*) aux;
        uitem->idx = idx++;
        aux = aux->next;
    }
    
    UserList_print(head);
    return 0; 


    

}

void Init_msg(Message* msg, char data[MSGLENGHT],int idx, ListHead* head, char username[MAXLENGHT])
{
    ListItem* aux = head->first;
    while(aux){
        UserListItem* uitem = (UserListItem*) aux;
        if(uitem->idx == idx){
            strcpy(msg->reciver,uitem->user.username);
        }
        aux = aux->next;
    }
    strcpy(msg->sender,username);   
    strcpy(msg->data,data);

}

void store(thread_args_t *targs, Message *msg)
{  
    if (!strcmp(targs->user->username, msg->reciver))
    {
        Inbox *mitem = malloc(sizeof (Inbox));
        strcpy(mitem->msg.sender,  msg->sender);
        strcpy(mitem->msg.reciver, msg->reciver);
        strcpy(mitem->msg.data,    msg->data);
        
        ListItem *result =
            List_insert(targs->inbox, targs->inbox->last, (ListItem *)mitem);
        assert(result);
    }
}
char *input(thread_args_t *targs)
{   // wait for user input and store incoming messages
    fflush(stdout);
    fd_set fds, fdr;
    FD_ZERO(&fds);
    FD_SET(0, &fds);                // add STDIN to the fd set
    FD_SET(targs->sockfd, &fds);    // add socket to the fd set
    for (; ; )
    {
        if (fdr = fds, select(targs->sockfd+1, &fdr, NULL, NULL, NULL) < 0)
            perror("select"), exit(1);
        if (FD_ISSET(0, &fdr))
        {   // this is the user's input
            static char data[MSGLENGHT];
            if (!fgets(data, sizeof data, stdin)) return NULL;  // no more user input
            data[strlen(data)-1] = '\0';
            return data;
        }
        // if no user input, then there's a message
        Message msg;
        socklen_t len = sizeof(targs->servaddr);
        if (recvfrom(targs->sockfd, &msg, sizeof msg, 0,
                     (struct sockaddr *)targs->servaddr, &len) < 0)
            perror("An error occurred while reciving Message from the Server"), exit(1);
        store(targs, &msg);
    }
}

void LL_freer(ListHead* head){
    ListItem* aux = head->first;
    while(aux){
        ListItem* a = aux;
        aux= aux->next;
        free(a);
    }

    List_init(head);
}


pthread_t thread = { 0 };

int main(int argc, char ** argv) {
    int sockfd;
    char username[MAXLENGHT];
    struct sockaddr_in  servaddr;
    ListHead inbox;
    List_init(&inbox);   

    thread_args_t targs = {0};
    User user;
    ListHead on_list;
    List_init(&on_list);
  

    //creating socket file descriptor
    if( (sockfd = socket(AF_INET,SOCK_DGRAM, 0)) < 0 ){
        perror("socket creation failed!");
        exit(EXIT_FAILURE);
    }

    memset(&servaddr,0, sizeof(servaddr));

    //filling server info
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(PORT);
    servaddr.sin_addr.s_addr = inet_addr(argv[1]);

    user.connected = authentication(&user, sockfd, servaddr, username);
    
    if(user.connected == 0){
        printf("\n\t\t\t\t----------Welcome----------");
        targs.user = &user;
        targs.inbox = &inbox;
        targs.sockfd = sockfd;
        targs.servaddr = &servaddr; 
    }
    while(user.connected == 0){
        
        int ret, op, written_bytes, read_bytes;
        Transfer tran;
        size_t len;

        printf("\nPlease choose an option: ");
        printf("\n1.Send a message!");
        printf("\n2.Incoming messages!");
        printf("\n3.Logout!");
        printf("\nYour choice:\t");
        char *str = input(&targs);
        sscanf(str, "%d", &ret);
        
        len = offsetof(Transfer,payload);
        socklen_t addrlen = sizeof(servaddr);

        if (ret == 1)
        {
            
            int res, size, id;
            op = 3;
            tran.op = op;

            written_bytes = sendto(sockfd, &tran, len, MSG_CONFIRM,
                   (struct sockaddr *)&servaddr, sizeof(servaddr));
            if(written_bytes == -1){
                perror("An error occurred while sending tran to the Server");
                exit(1);
            } 
            
            // We cannot preclude that a message arrives here,
            // therefore we must handle that case.
            Message msg;
            while ((read_bytes = recvfrom(sockfd, &msg, sizeof msg, 0,
                                          (struct sockaddr *)&servaddr,
                                          &addrlen)) == sizeof msg)
                store(&targs, &msg);
            if (read_bytes == -1) perror("An error occurred while reciving Message from the Server"), exit(1);
            size = *(int *)&msg;
            
            
            res = recv_list(sockfd, servaddr, &on_list, size, user.username);
            
            if (res == 0)
            {
                printf("\nChoose whom you want to send a message to");
                printf("\nYour choice:\t");
                str = input(&targs);

                sscanf(str, "%d", &id);

                printf("\nWrite the message you want to send:\n");
                str = input(&targs);

                Init_msg(&tran.payload.msg, str, id, &on_list, user.username);
                len += sizeof(Message);
                tran.op = 4;

                written_bytes = sendto(sockfd, &tran, len, MSG_CONFIRM,
                                           (struct sockaddr *)&servaddr, sizeof(servaddr));
                if (written_bytes == -1) perror("An error occurred while sending Message to the Server"), exit(1);
                // We cannot preclude that a message arrives here,
                // therefore we must handle that case.
                while ((read_bytes = recvfrom(sockfd, &msg, sizeof msg, 0,
                                              (struct sockaddr *)&servaddr,
                                              &addrlen)) == sizeof msg)
                    store(&targs, &msg);
                if (read_bytes == -1) perror("An error occurred while reciving Message from the Server"), exit(1);
                int sent = *(int *)&tran.payload.msg;
                if (sent == 0) printf("\nMessage sent!");
                else if (sent == 1) printf("\nUser is offline :(");
            }
            else if (res == 1) printf("\nNo online user :(");
            LL_freer(&on_list);
        }
        else if (ret == 2) Print_msg(&inbox);
        else if(ret == 3)
        {
            op = 5;
            tran.op = op;
           
            /* written_bytes = sendto(sockfd, &op, sizeof op, MSG_CONFIRM,
                   (struct sockaddr *)&servaddr, len);
             */
            /* written_bytes = sendto(sockfd,&user,sizeof(User),MSG_CONFIRM,
                    ( struct sockaddr *)&servaddr, len) */;

            len += sizeof(User);
            tran.payload.user = user;
            written_bytes = sendto(sockfd,&tran,len,MSG_WAITALL,(struct sockaddr *)&servaddr, sizeof(servaddr));
            if( written_bytes == -1)perror("An error occurred while sending User info to the Server"), exit(1);
            int ok,read_bytes = recvfrom(sockfd,(int *)&ok,
                                  sizeof(int),MSG_WAITALL,(struct sockaddr* )&servaddr, &addrlen );
            if(read_bytes < 0) perror("An error occurred while reciving Message from the Server"), exit(1);
            if(ok = 1)
            {
                user.connected = 1;
                LL_freer(&inbox);
                LL_freer(&on_list);
                printf("User logging out!\n\t\t\t\t Bye Bye :D\n");    
            }

        }      
            
            

        

    }    
    
    close(sockfd);

    return 0;


}