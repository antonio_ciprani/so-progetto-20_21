#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stddef.h>

#include "linked_list.h"

#define PORT 8080
#define MAXLINE 1024
#define MAXLENGHT 50
#define MSGLENGHT 256
#define NUM_THREADS 2

typedef struct User{
    char username[MAXLENGHT];
    char password[MAXLENGHT];
    int connected;
    struct sockaddr_in cliaddr;

}User ;

typedef struct UserListItem {
  ListItem list;
  User user;
  int idx;

} UserListItem;


typedef struct Msg{

  char sender[MAXLENGHT];
  char reciver[MAXLENGHT];
  char data[MSGLENGHT];
}Message;

/* typedef struct thread_args_s{
  ListHead conn_head;
  ListHead head;
  int sockfd;
  struct sockaddr_in cliaddr;
  int op;
} thread_args_t; */


typedef union Payload{
  User user;
  Message msg;
}Payload;

typedef struct Transfer{
  int op;
  Payload payload;
}Transfer;





//UserList manipolation
void UserListItem_init(UserListItem* item, User user);
int LoadUsers(ListHead* head);

void UserList_print(ListHead * head);
UserListItem* List_find_by_username(ListHead* head, char username[MAXLENGHT]);
void Connected_list(ListHead* head, ListHead* conn_head);
void Set_index(ListHead* head);
void Send_list(ListHead* head, int sockfd,struct sockaddr_in cliaddr, int size);


//authentication
int Login(User user, int sockfd, struct sockaddr_in cliaddr, ListHead* head);
int Signup(User user, int sockfd, struct sockaddr_in cliaddr, ListHead* head);
void Set_cliaddr(ListHead* head, char username[MAXLENGHT],struct sockaddr_in cliaddr);
void Set_connected(ListHead* head, char username[MAXLENGHT]);
void disconnect(ListHead* head,ListHead* conn_head,User user,struct sockaddr_in cliaddr,int sockfd);

void * server_thread(void* args);