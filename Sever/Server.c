#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <assert.h>
#include <semaphore.h>
#include <pthread.h>

#include "Server.h"



//function responsible of sending to the client that request's it the list of online users
void Send_list(ListHead* head, int sockfd,struct sockaddr_in cliaddr, int size){
    int written_bytes;
    socklen_t len = sizeof(cliaddr);
    char username[MAXLENGHT];
    
    
    if(head->size == 1){
        return;
    }

    ListItem* aux = head->first;
    for(int i=0;i<size;i++){
        memset(username,0,sizeof(username));
        UserListItem* uitem = (UserListItem*) aux;
        strcpy(username,uitem->user.username);
        do{
            written_bytes = 0;
            written_bytes = sendto(sockfd,(const char *)username,strlen(username),0,(const struct sockaddr*)&cliaddr,len); 
            if(written_bytes == -1) perror("An error occurred while sending username to the Client"),exit(1);;
        }while(written_bytes != strlen(username));
        printf("\nusername mandato: %s",username);
        printf("\n");
        if(aux->next){
            aux = aux->next;
        }
    }
}




/*function responsible of creating the link_list of online users from the link_list
of all the registred users*/

void Connected_list(ListHead* head, ListHead* conn_head){
    ListItem* aux = head->first;
    while(aux){
        UserListItem* uitem = (UserListItem*)aux;
        if(uitem->user.connected == 1){    
            UserListItem* aux_user = (UserListItem*)malloc(sizeof(UserListItem));
            if(conn_head->size == 0){
                UserListItem_init(aux_user, uitem->user);
                ListItem* result = List_insert(conn_head,conn_head->last,(ListItem*)aux_user);
                assert(result);
            
            }
            else{
                if((List_find_by_username(conn_head,uitem->user.username)) == 0){
                    UserListItem_init(aux_user,(User) uitem->user);
                    ListItem* result = List_insert(conn_head,conn_head->last,(ListItem*)aux_user);
                    assert(result);
                    
                }
            }
        
        }

        aux = aux->next;
    }
  
    
}
//function that sets the connected flag of a user to true

void Set_connected(ListHead* head, char username[MAXLENGHT]){
    ListItem* aux = head->first;
    while(aux){
        UserListItem* uitem = (UserListItem*) aux;
        if(!strcmp(uitem->user.username,username))
            uitem->user.connected = 1;
        aux = aux->next;
    }

    
    

}

UserListItem* List_find_by_username(ListHead* head, char username[MAXLENGHT]){
    ListItem* aux = head->first;
    while(aux){
        UserListItem* uitem = (UserListItem*) aux;
        if(!strcmp(uitem->user.username,username))
            return uitem;
        aux = aux->next;
    }
    return 0;
}

void UserList_print(ListHead * head){
    ListItem* aux = head->first;
    char* some_addr;
    while(aux){
        UserListItem * uitem = (UserListItem*) aux;
        printf("\nUsername: %s", uitem->user.username);
        printf("\nPassword: %s", uitem->user.password);
        printf("\nOnline: %d", uitem->user.connected);
        printf("\nIdx: %d",uitem->idx);
        some_addr = inet_ntoa(uitem->user.cliaddr.sin_addr); // return the IP
        printf("\nclient ip: %s\n", some_addr);
        printf("\n");
        aux = aux->next;
    }
}

void UserListItem_init(UserListItem* item, User user){
    item->list.prev = 0;
    item->list.next = 0;
    strcpy(item->user.username, user.username);
    strcpy(item->user.password, user.password);
    item->user.connected = user.connected;
    memcpy(&item->user.cliaddr,&user.cliaddr, sizeof(item->user.cliaddr));
    

}

void Set_cliaddr(ListHead* head, char username[MAXLENGHT],struct sockaddr_in cliaddr){
    ListItem* aux = head->first;
    while (aux){
        UserListItem* uitem = (UserListItem*) aux;
        if(!strcmp(uitem->user.username,username)){
            memcpy(&uitem->user.cliaddr,&cliaddr,sizeof(uitem->user.cliaddr));
        }
        aux = aux->next;

    }
    
}
//function that loads up the users in the user's list from the db

int LoadUsers(ListHead* head){
    User usr;
    FILE *fp;

    fp = fopen("Users.db", "r+");
    while(fread(&usr, sizeof(User),1,fp)){
        
        UserListItem * user_item = (UserListItem*)malloc(sizeof(UserListItem));
        UserListItem_init(user_item, usr);
        ListItem* result = List_insert(head,head->last,(ListItem*)user_item );
        assert(result);
        memset(&usr,0,sizeof(usr));

    }
    fclose(fp);
    return 0;

}
//function that handles the login request from clients

int Login(User user, int sockfd, struct sockaddr_in cliaddr, ListHead* head){
    User usr;
    FILE *fp;
    int res;
    
    fp = fopen("Users.db", "r");
    
    while(fread(&usr,sizeof(User),1,fp)){
        if(!strcmp(user.username,usr.username)){
            if(!strcmp(user.password,usr.password)){
                printf("\nUser found!\n");
                Set_connected(head, user.username);
                Set_cliaddr(head,user.username,cliaddr);
                res = 0;
                sendto(sockfd,(int *)&res, sizeof(long int),MSG_CONFIRM, (const struct sockaddr *)&cliaddr, sizeof(cliaddr));
                fclose(fp);
                return res;

            }
            else{
                printf("\nInvalid Password!");
                res = 1;
                sendto(sockfd,(int *)&res, sizeof(long int),MSG_CONFIRM, (const struct sockaddr *)&cliaddr, sizeof(cliaddr));
                fclose(fp);
                return res;
            }
        }
        
    }
    
    printf("Invalid Username!");
    res = 2;
    sendto(sockfd,(int *)&res, sizeof(long int),MSG_CONFIRM, (const struct sockaddr *)&cliaddr, sizeof(cliaddr));
    fclose(fp);
    return res;
    

    

}

//function that handles the singup request from clients
int Signup(User user, int sockfd, struct sockaddr_in cliaddr, ListHead* head){
    FILE *fp;
    int res;
    int temp;

    fp = fopen("Users.db", "a");
    int written_bytes = fwrite(&user,sizeof(User),1,fp);
    if(written_bytes > 0){
        UserListItem* user_item = (UserListItem*)malloc(sizeof(UserListItem));
        UserListItem_init(user_item, user);
        user_item->user.connected = 1;
        memcpy(&user_item->user.cliaddr,&cliaddr,sizeof(user_item->user.cliaddr));
        ListItem* result = List_insert(head, head->last, (ListItem*)user_item);
        assert(result);
        res = 0;
        sendto(sockfd,(int *)&res, sizeof(long int),MSG_CONFIRM, (const struct sockaddr *)&cliaddr, sizeof(cliaddr));
        printf("\nUser signed up!\n");
        fclose(fp);
        return res;
        
    }
    else{
        res = 1;
        sendto(sockfd,(int *)&res, sizeof(long int),MSG_CONFIRM, (const struct sockaddr *)&cliaddr, sizeof(cliaddr));
        fclose(fp);
        return res;
    }
    
    


}

//function responsible of sending a message to the reciver specifed in the message
void Send_msg(Message* msg, int sockfd, ListHead* head){
    UserListItem* reciver = List_find_by_username(head,msg->reciver);
    UserListItem* sender = List_find_by_username(head,msg->sender);
    int ret;
    if(reciver == 0){
        ret = 1;
        sendto(sockfd,(const int *)&ret, sizeof(int),MSG_CONFIRM, (const struct sockaddr *)&sender->user.cliaddr, sizeof(sender->user.cliaddr));
        
    }
    else{
    
        int written_bytes;
        do{
            written_bytes = 0;
            written_bytes = sendto(sockfd,msg,sizeof(Message),MSG_CONFIRM,(const struct sockaddr *)&reciver->user.cliaddr, sizeof(reciver->user.cliaddr));
            if(written_bytes == -1) perror("An error occurred while sending Message from the Client"),exit(1);
        }while(written_bytes != sizeof(Message));
        
        
        ret = 0;
        sendto(sockfd,(const int *)&ret, sizeof(int),MSG_CONFIRM, (const struct sockaddr *)&sender->user.cliaddr, sizeof(sender->user.cliaddr));
       
    }

    


}

//function that recvies the message from the sender user and then calls the Send_msg function
void Forward_message(int sockfd, ListHead* head, struct sockaddr_in cliaddr){
    Message msg = {0}; 
    fd_set fds, fdr;
    struct timeval timeout;

    FD_ZERO(&fds);
    FD_SET(sockfd, &fds);
    
    timeout.tv_sec = 40;
    timeout.tv_usec = 0;

    int len = sizeof(cliaddr);
    
    if(head->size >1){
        int read_bytes;
        if(fdr = fds,select(sockfd+1,&fdr, NULL,NULL,&timeout) > 0 ){
            if(FD_ISSET(sockfd, &fdr)){ 
                do{
                    read_bytes = 0;
                    read_bytes = recvfrom(sockfd,(Message *)&msg, sizeof(Message),MSG_WAITALL,(struct sockaddr* )&cliaddr, &len );
                    if(read_bytes == -1)perror("An error occurred while receiving Message from the Client"),exit(1);
                }while( (read_bytes != sizeof(Message))  );
                
                Send_msg(&msg,sockfd,head);
                printf("\nmessage forwarded\n");
            }
        }
        else{
            perror("select in Forward_message");
        }    
    }

}

void disconnect(ListHead* head,ListHead* conn_head,User user,struct sockaddr_in cliaddr,int sockfd){
    socklen_t len = sizeof cliaddr;
    ListItem* aux = head->first;
    while(aux){
        UserListItem* uitem = (UserListItem*) aux;
        if(!strcmp(uitem->user.username,user.username))
            uitem->user.connected = 0;
        aux = aux->next;
    }
    UserListItem* uitem = List_find_by_username(conn_head, user.username);
    ListItem * result = List_detach(conn_head,(ListItem*)uitem);
    assert(result);
    int res = 1;
    sendto(sockfd,(int*)&res,sizeof(int),
            MSG_CONFIRM,(struct sockaddr *)&cliaddr,len );
    
}
void disconnect_na(ListHead* head,ListHead* conn_head,User user){
    ListItem* aux = head->first;
    while(aux){
        UserListItem* uitem = (UserListItem*) aux;
        if(!strcmp(uitem->user.username,user.username))
            uitem->user.connected = 0;
        aux = aux->next;
    }
    UserListItem* uitem = List_find_by_username(conn_head, user.username);
    if(uitem != 0){    
        ListItem * result = List_detach(conn_head,(ListItem*)uitem); 
        assert(result);
    }    
    /*int res = 1;
     sendto(sockfd,(int*)&res,sizeof(int),
            MSG_CONFIRM,(struct sockaddr *)&cliaddr,len ); */
    
}
//thread function that handles all server activity



int main() {

    int sockfd;
    char buffer[MAXLINE];
    char *hello = "Hello from Server!";
    char* some_addr;
    struct sockaddr_in servaddr;
    int running = 0;

    ListHead head;
    ListHead conn_head;
    List_init(&conn_head);
    List_init(&head);
    //thread_args_t targs = { 0 };

    

    if((LoadUsers(&head)) == 1){
        printf("An error occured while loading the Users");
    }

    UserList_print(&head);
    
    //creating socket file descriptor  
    if( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0){
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    } 


    memset(&servaddr, 0 , sizeof(servaddr));
   

    //filling server information
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = htons(PORT);

    //bind the socket with  server address
    if( bind(sockfd,(const struct sockaddr *) &servaddr ,sizeof(servaddr)) < 0){
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    
    int n;


    while(1){
        
        struct sockaddr_in current_cliaddr;
        socklen_t addrlen;
        Transfer tran;
        ssize_t len;
        int written_bytes;

        memset(&tran,0,sizeof(Transfer));
        memset(&current_cliaddr, 0 , sizeof(current_cliaddr));  

        printf("\nServer Cycle...\n");

        addrlen = sizeof(current_cliaddr);
        len = recvfrom(sockfd,&tran,sizeof(Transfer),MSG_WAITALL,(struct sockaddr* )&current_cliaddr,&addrlen);
        
        if(len < 0){
            perror("Error in recvfrom");
            exit(1);
        }

        len -= offsetof(Transfer,payload);
        User user;
        Message msg;

        memset(&user,0,sizeof(User));
        memset(&msg,0,sizeof(Message));

        switch(tran.op){
        case 1:

            if(len != sizeof(User)){
                fprintf(stderr,"recvfrom: error -- len=%zd User=%zu\n",
                    len,sizeof(User));  
                break;
            }    

            user = tran.payload.user;
            Signup(user,sockfd,current_cliaddr,&head);
            Connected_list(&head, &conn_head);
            printf("\nOnline Users: ");
            UserList_print(&conn_head);
            break;

        case 2:
            

            if(len != sizeof(User)){
                fprintf(stderr,"recvfrom: error -- len=%zd User=%zu\n",
                    len,sizeof(User));
                break;      
            }
            user = tran.payload.user;
            disconnect_na(&head,&conn_head,user);

            Login(user,sockfd,current_cliaddr,&head);
            Connected_list(&head, &conn_head);
            printf("\nOnline Users: ");
            UserList_print(&conn_head);
            break;

        case 3:
            
            Connected_list(&head, &conn_head);

            int size = conn_head.size;

            written_bytes = sendto(sockfd,(int*)&size,sizeof(int),MSG_CONFIRM,(const struct sockaddr*)&current_cliaddr,addrlen); 
            if(written_bytes < 0) {
                perror("Error while sending list size to the Client");
                break;
            }
            printf("\nsize mandata: %d", size);

            Send_list(&conn_head,sockfd,current_cliaddr,size);

            break;

        case 4:
            
            
            
            if(len != sizeof(Message)){
                fprintf(stderr,"recvfrom: error -- len=%zd Message=%zu\n",
                    len,sizeof(Message));
                    break;
            } 
                  
            
            msg = tran.payload.msg;
            Send_msg(&msg,sockfd,&conn_head);
            printf("\nmessage forwarded\n");
            break;

        case 5: 

            
            if(len != sizeof(User)){
                fprintf(stderr,"recvfrom: error -- len=%zd User=%zu\n",
                    len,sizeof(User));      
                break;
            }
            user = tran.payload.user;
            disconnect(&head,&conn_head,user,current_cliaddr,sockfd);
            printf("\nOnline Users: ");
            UserList_print(&conn_head); 
            break;
        
        default:

            fprintf(stderr,"recvfrom: unknown command -- op=%d\n",tran.op);
            break;
        
        

        }
    
    



        
    }

 


}