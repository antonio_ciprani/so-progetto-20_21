# UDP-MSG eXchanger
A private chat environment, that uses a Server-Client UDP architecture
to exchange *text messages* between hosts.
Using a simple authentication system to handle login and sign up and after that allows online users to exchange text messages inside a private chat.

# How to install

1. Clone this repository
2. Enter the Server directory, open a terminal and run the `make` command
3. Enter the Client directory, open a terminal and run the `make` command

# How to run

1. Open a terminal inside the Server directory and run the `./Server` command
2. Open a terminal inside the Client directory and run the `./Client` command followed by the IP address of the Server (e.g `./Client 127.0.0.1`)  

# On Client

The interface is self explanatory and easy to understand, it will guide you through


